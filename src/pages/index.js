import reset from "styled-reset";
import { createGlobalStyle } from "styled-components";
import Contents from "../components/Contents";

const GlobalStyle = createGlobalStyle`
  ${reset}
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <Contents />
    </>
  );
}

export default App;
