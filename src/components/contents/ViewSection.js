import styled from "styled-components";
import { SectionBox } from "../common/Common";

const WrapperSection = styled(SectionBox)`
  height: 430px;
  padding-top: 60px;
  background-image: linear-gradient(103deg, #020681 38%, #1033a1 68%);
  color: var(--white);

  @media screen and (min-width: 768px) {
    height: 530px;
    background-image: url("/assets/topbg.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 50% 50%;
  }
`;

const ContainerBox = styled.div`
  h2 {
    padding-top: 20px;
    font-size: 32px;
    font-weight: var(--bold);
    text-align: center;
    line-height: 1.3;
    letter-spacing: -1.3px;
  }

  p {
    margin-top: 10px;
    font-weight: var(--normal);
    font-size: 12px;
    text-align: center;
    line-height: 1.6;
    color: var(--white);
  }

  @media screen and (min-width: 768px) {
    h2 {
      font-size: 36px;
      line-height: 1.4;
    }

    p {
      font-size: 14px;
      line-height: 1.7;
    }
  }
`;

const LinkBox = styled.div`
  width: 180px;
  height: 40px;
  margin: 22px auto 0;
  border-radius: 23px;
  font-weight: var(--bold);
  color: var(--blue_300);
  background-color: var(--white);
  box-shadow: -3.3px -4.1px 14.9px 0 rgba(0, 47, 103, 0.6), 0 6px 10px 0 rgba(12, 4, 97, 0.6);

  a {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

const PreviewImageBox = styled.div`
  position: absolute;
  top: 284px;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 352px;
  height: 236px;
  margin: auto;
  background-image: url("/assets/view-mobile.png");
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: 50% 50%;

  button {
    width: 68px;
    height: 54px;
    background-image: url("/assets/play-btn-hover.png");
    background-size: contain;
  }

  @media screen and (min-width: 768px) {
    top: 320px;
    width: 546px;
    height: 350px;

    button {
      width: 75px;
      height: 60px;
    }
  }
`;

function ViewSection() {
  return (
    <WrapperSection bgColor={"transparents"}>
      <ContainerBox>
        <h2>
          쇼핑몰 셀러의
          <br /> 간편 매출정산 서비스
        </h2>
        <p>
          포기했던 쇼핑몰 매출 정산금 흐름 파악, 엑셀은 이제 그만!
          <br /> 장사왕이 바로 알려드립니다.
        </p>
      </ContainerBox>
      <LinkBox>
        <a href='#top'>무료 베타 시작하기</a>
      </LinkBox>
      <PreviewImageBox>
        <button>
          <span className='blind'>youtube 바로가기</span>
        </button>
      </PreviewImageBox>
    </WrapperSection>
  );
}

export default ViewSection;
