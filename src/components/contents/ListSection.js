import React from "react";
import styled from "styled-components";
import { SectionBox } from "../common/Common";

const WrapperSection = styled(SectionBox)`
  background-color: var(--blue_100);

  @media screen and (min-width: 768px) {
    background-color: var(--white);
    .pc {
      display: flex;
      flex-direction: column;
    }
  }
`;
const ContainerBox = styled.div`
  display: flex;
  flex-direction: column-reverse;
  gap: 4px;
  padding: 110px 0 20px;

  h2 {
    font-size: 24px;
    font-weight: var(--bold);
    text-align: center;
    line-height: 28px;
    color: var(--blue_400);
  }

  p {
    font-size: 12px;
    font-weight: var(--bold);
    text-align: center;
    color: var(--blue_200);
  }

  @media screen and (min-width: 768px) {
    padding-top: 180px;
    background-color: var(--white);

    h2 {
      font-size: 22px;
    }

    p {
      font-size: 14px;
    }
  }
`;

const WrapperListBox = styled.div`
  padding-bottom: 70px;

  @media screen and (min-width: 768px) {
    padding-bottom: 0;
    display: flex;
    gap: 8px;
    justify-content: center;
  }
`;

const ListBox = styled.div`
  h3 {
    display: none;
  }

  @media screen and (min-width: 768px) {
    width: 370px;
    height: auto;
    padding: 22px;
    border-radius: 10px;

    h3 {
      display: block;
      width: 58px;
      height: 12px;
      margin: 0 auto 12px;
      background-size: contain;
      background-repeat: no-repeat;
      background-position: 50%;
    }
  }
`;
const AsIsListBox = styled(ListBox)`
  @media screen and (min-width: 768px) {
    border: solid 1px #e7e7e7;
    box-shadow: 2px 3px 15px 0 rgba(58, 83, 98, 0.1), -3px -1px 15px 0 rgba(61, 82, 94, 0.08);
    background-color: var(--gray_000);
    h3 {
      background-image: url("/assets/text-asis.svg");
    }
  }
`;
const ToBeListBox = styled(ListBox)`
  @media screen and (min-width: 768px) {
    box-shadow: 5px 5px 25px 0 rgba(0, 50, 148, 0.15), -5.3px -5.3px 15px 0 rgba(68, 206, 250, 0.1),
      0 0 3px 0 rgba(19, 38, 67, 0.15);
    background-color: var(--blue_050);
    h3 {
      background-image: url("/assets/text-tobe.svg");
    }
  }
`;

const List = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
  height: auto;

  li {
    height: auto;
    border-radius: 16px;
    background-color: var(--white);

    h4 {
      font-weight: var(--bold);
      line-height: 16px;
    }
    p {
      line-height: 1.4;
    }
  }
`;

const AsIsItem = styled.li`
  height: auto;
  padding: 12px 13px 20px;
  margin-top: -20px;
  box-shadow: 2px 3px 15px 0 rgba(58, 83, 98, 0.1), -3px -1px 15px 0 rgba(61, 82, 94, 0.08);

  &:first-child {
    margin-top: 10px;
  }
  &:nth-child(2n-1) {
    width: 280px;
    align-self: flex-start;
  }
  &:nth-child(2n) {
    width: 240px;
    align-self: flex-end;
  }

  h4 {
    letter-spacing: -0.5px;
    font-size: 12px;
    color: var(--blue_400);
  }

  p {
    margin-top: 2px;
    font-size: 10px;
  }
`;
const ToBeItem = styled.li`
  position: relative;
  width: 320px;
  padding: 12px 16px;
  box-shadow: 1.3px 1.9px 9.6px 0 rgba(58, 83, 98, 0.1),
    -1.9px -0.6px 9.6px 0 rgba(61, 82, 94, 0.08);

  h4 {
    font-size: 16px;
    color: var(--blue_200);
  }

  p {
    max-width: 236px;
    margin-top: 4px;
    font-size: 12px;
    line-height: 1;
  }
`;

const ImojiBox = styled.div`
  background-image: ${props => 'url("/assets/' + props.name + "-" + props.num + '.svg")' || "none"};
  background-size: contain;
  background-repeat: no-repeat;
  background-position: 50% 50%;
`;
const AsIsImojiBox = styled(ImojiBox)`
  display: inline-block;
  width: 13px;
  height: 13px;
  vertical-align: middle;
  margin-top: -3px;
  margin-left: 2px;
`;
const ToBeImojiBox = styled(ImojiBox)`
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  right: 24px;
  display: block;
  width: 32px;
  height: 32px;
`;

const PointBox = styled.div`
  padding-top: 50px;
  font-size: 18px;
  font-weight: var(--bold);
  line-height: 1.55;
  text-align: center;
  color: ${props => props.fontCol || "var(--blue_200)"};
  background-image: url("/assets/img-dots.svg");
  background-repeat: no-repeat;
  background-position: 50% 18px;

  @media screen and (min-width: 768px) {
    width: 270px;
    margin: auto;
    padding-bottom: 8px;
    font-size: 16px;
    line-height: 1.37;
  }
`;

function ListSectionToBe() {
  return (
    <WrapperSection>
      <ContainerBox>
        <h2>
          장사왕을 선택하면
          <br className='mobile' /> 경쟁력이 달라집니다
        </h2>
        <p>똑똑하게 판매하는 온라인 셀러가 되자!</p>
      </ContainerBox>

      <WrapperListBox>
        <AsIsListBox>
          <h3>
            <span className='blind'>as-is</span>
          </h3>
          <List className='pc'>
            <AsIsItem>
              <h4>매일 쇼핑몰별 엑셀다운로드 후 합산해서 확인해요</h4>
              <p>
                매일 직원 1~2명이 하루에 1시간 이상 입점한 몰들에서
                <br /> 매출내역을 다운 받아 취합하고 엑셀을 활용해 총 매출금을 확<br />
                인해요.
                <AsIsImojiBox name={"asis"} num={"01"} aria-hidden='true' />
              </p>
            </AsIsItem>
            <AsIsItem>
              <h4>대략 감으로 예상만 하다 손해 볼 때도 있었어요</h4>
              <p>
                판매 일이 너무 바쁘기도하고, 엑셀이 사용이
                <br />
                익숙하지 않아 대략 감으로 예상만 해요. 그런데
                <br />
                많이 팔았다고 좋아했다가 몇개월 후 정리했을때
                <br />
                적자여서 당황스러울때도 있었어요.
                <AsIsImojiBox name={"asis"} num={"02"} aria-hidden='true' />
              </p>
            </AsIsItem>
            <AsIsItem>
              <h4>정산금액이 틀려도 이유를 찾기 어려워요</h4>
              <p>
                다양한 수수료와 입점몰마다 다른 정산일로 정확하게 정산금을
                <br />
                파악하기가 어렵고, 가끔은 예상했던 정산금과 입점몰에서 정<br />
                산해준 금액이 달라도 이유를 찾거나 파악하기가 어려워요.
                <AsIsImojiBox name={"asis"} num={"03"} aria-hidden='true' />
              </p>
            </AsIsItem>
            <AsIsItem>
              <h4>취합하려면 PC 앞에서만</h4>
              <p>
                쇼핑몰 관리자는 대부분 모바일로 확인이 불편하거나
                <br />
                아예 지원을 안하기때문에 아무리 급해도 확인하려면
                <br />
                PC 앞에서만 가능해요.
                <AsIsImojiBox name={"asis"} num={"04"} aria-hidden='true' />
              </p>
            </AsIsItem>
            <AsIsItem>
              <h4>통합 솔루션에서는 상품명을 모두 동일하게 올려야해요</h4>
              <p>
                주요 통합 솔루션에서 상품명이 다를 경우 매출 취합을 해도 별<br />
                개의 상품으로 처리돼요. 상품명이 다르면 취합해서 보기가 어<br />
                려워 상품명을 동일하게 처리하곤해요.
                <AsIsImojiBox name={"asis"} num={"05"} aria-hidden='true' />
              </p>
            </AsIsItem>
          </List>
          <PointBox fontCol={"var(--blue_400)"} className='pc'>
            <p>
              매출 총액과 정산금 파악의 어려움으로
              <br /> 자금 흐름 및 상품판매 전략의 실패율 높음
            </p>
          </PointBox>
        </AsIsListBox>
        <ToBeListBox>
          <h3>
            <span className='blind'>to-be</span>
          </h3>
          <List>
            <ToBeItem>
              <h4>계정 연동으로 자동 집계 </h4>
              <p>쇼핑몰 계정연동만으로 편리하게 매출 정산 내역을 바로 확인 가능합니다. </p>
              <ToBeImojiBox name={"tobe"} num={"01"} aria-hidden='true' />
            </ToBeItem>
            <ToBeItem>
              <h4>데이터의 이해를 돕는 대시보드 지원</h4>
              <p>
                매출, 정산금 변화의 이해를 도울 수 있도록 그래프와 차트등으로 구성된 대시보드를
                지원합니다.
              </p>
              <ToBeImojiBox name={"tobe"} num={"02"} aria-hidden='true' />
            </ToBeItem>
            <ToBeItem>
              <h4>매출별, 정산별, 상품별 분석 가능</h4>
              <p>
                매출별, 정산별, 상품별 분석을 통해 다각도로 분석하여 상품판매를 극대화 할 수
                있습니다.
              </p>
              <ToBeImojiBox name={"tobe"} num={"03"} aria-hidden='true' />
            </ToBeItem>
            <ToBeItem>
              <h4>다양한 디바이스 지원 </h4>
              <p>모바일, 태블릿 등 다양한 디바이스에서 언제 어디서나 서비스이용이 가능합니다.</p>
              <ToBeImojiBox name={"tobe"} num={"04"} aria-hidden='true' />
            </ToBeItem>
            <ToBeItem>
              <h4>상품 그룹 관리 지원</h4>
              <p>
                동일 상품이지만 판매를 높이기 위해 입점몰마다 다르게 올린상품을 묶어 그룹으로 매출을
                확인하고 상품 그룹 분석을 확인 할 수 있습니다.
              </p>
              <ToBeImojiBox name={"tobe"} num={"05"} aria-hidden='true' />
            </ToBeItem>
          </List>
          <PointBox>
            <p>
              쉽고 빠르게 어디서나
              <br className='mobile' /> 매출, 정산금과 <br className='pc' />
              상품 분석 가능으로
              <br className='mobile' /> 판매전략 수립에 용이
            </p>
          </PointBox>
        </ToBeListBox>
      </WrapperListBox>
    </WrapperSection>
  );
}

export default ListSectionToBe;
