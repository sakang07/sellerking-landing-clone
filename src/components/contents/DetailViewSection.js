import styled from "styled-components";
import { BoldSpan, SectionBox } from "../common/Common";
import { useMediaQuery } from "usehooks-ts";

const WrapperSection = styled(SectionBox)`
  padding-top: 20px;
  margin-bottom: 80px;

  @media screen and (min-width: 768px) {
    margin-bottom: 50px;
  }
`;

const FeatureItem = styled.li`
  text-align: center;

  h3 {
    padding: 50px 0 8px;
    font-size: 20px;
    font-weight: var(--bold);
    color: var(--blue_200);
  }
  p {
    font-size: 14px;
    line-height: 1.6;
  }
  img {
    display: block;
    width: 234px;
    height: auto;
    margin: 6px auto 0;
  }

  @media screen and (min-width: 768px) {
    h3 {
      padding-bottom: 2px;
    }
    img {
      width: 560px;
    }
  }
`;

function DetailViewSection() {
  const matches = useMediaQuery("(min-width: 768px)");

  return (
    <WrapperSection>
      <h2 className='blind'>장사왕의 주요 화면을 소개합니다</h2>
      <ul>
        <FeatureItem>
          <h3>쇼핑몰별 매출/정산금현황</h3>
          <p>
            쇼핑몰&nbsp;
            <BoldSpan>
              전체 매출과 몰별 매출 및 정산액을 <br className='mobile' />
              비교분석
            </BoldSpan>
            하며 볼 수 있습니다.
          </p>
          <img
            src={!matches ? "/assets/detail-01.png" : "/assets/01-t.png"}
            alt='쇼핑몰별 매출/정산금현황 화면 미리보기'
          />
        </FeatureItem>
        <FeatureItem>
          <h3>정산금 캘린더</h3>
          <p>
            캘린더 형태로 <BoldSpan>이전, 현재, 미래 정산예정금</BoldSpan>을
            <br className='mobile' /> 확인하실 수 있습니다.
          </p>
          <img
            src={!matches ? "/assets/detail-02.png" : "/assets/02-t.png"}
            alt='정산금 캘린더 화면 미리보기'
          />
        </FeatureItem>
        <FeatureItem>
          <h3>상세분석 차트 지원</h3>
          <p>
            매출별, 정산별, 상품별 분석으로
            <br className='mobile' /> <BoldSpan>매출의 극대화</BoldSpan>를 돕습니다.
          </p>
          <img
            src={!matches ? "/assets/detail-03.png" : "/assets/03-t.png"}
            alt='상세분석 차트 지원 화면 미리보기'
          />
        </FeatureItem>
        <FeatureItem className='pc'>
          <h3>상품 그룹 관리</h3>
          <p>
            동일한 상품을 몰마다 다른 이름으로 올렸어도&nbsp;
            <BoldSpan>한 상품으로 묶어서 관리</BoldSpan> 합니다.
          </p>
          <img src='/assets/04-t.png' alt='모바일 지원 화면 미리보기' />
        </FeatureItem>
        <FeatureItem>
          <h3>정산 중심의 주문내역확인</h3>
          <p>
            매출별, 정산별 해당하는 <BoldSpan>상품 상세 데이터</BoldSpan>를
            <br className='mobile' /> 확인하실 수 있습니다.
          </p>
          <img
            src={!matches ? "/assets/detail-04.png" : "/assets/05-t.png"}
            alt='정산 중심의 주문내역확인 화면 미리보기'
          />
        </FeatureItem>
        <FeatureItem className='pc'>
          <h3>모바일 지원</h3>
          <p>
            <BoldSpan>언제 어디서나 모바일</BoldSpan>로 매출 정산 내역을 확인하실 수 있습니다.
          </p>
          <img src='/assets/06-t.png' alt='모바일 지원 화면 미리보기' />
        </FeatureItem>
      </ul>
    </WrapperSection>
  );
}

export default DetailViewSection;
