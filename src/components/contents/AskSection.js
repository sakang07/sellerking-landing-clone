import styled from "styled-components";
import { SectionBox } from "../common/Common";

const ContainerBox = styled.div`
  h2 {
    padding: 64px 0 10px;
    font-size: 24px;
    font-weight: var(--bold);
    text-align: center;
    color: var(--blue_400);

    @media screen and (min-width: 768px) {
      font-size: 22px;
    }
  }

  p {
    font-size: 14px;
    line-height: 24px;
    text-align: center;
    color: var(--grey_200);
  }
`;

const FormBox = styled.div`
  width: 320px;
  margin: 24px auto 0;
  padding-bottom: 60px;
  font-size: 14px;

  @media screen and (min-width: 768px) {
    width: 600px;
  }
`;

const InputField = styled.fieldset`
  input {
    width: 100%;
    height: 44px;
    margin-bottom: 12px;
    padding: 14px;
    border: 1px solid var(--gray_050);
  }

  textarea {
    width: 100%;
    height: 250px;
    margin-bottom: 24px;
    padding: 14px;
    border: 1px solid var(--gray_050);
    line-height: 1.4;
  }
`;
const LoginField = styled.fieldset`
  input {
    width: 100%;
    height: 50px;
    border-radius: 25px;
    font-size: 15px;
    font-weight: var(--bold);
    color: var(--white);
    box-shadow: -4px -5px 18px 0 rgba(0, 96, 209, 0.2), 4px 4px 15px 0 rgba(15, 0, 184, 0.2);
    background-image: linear-gradient(97deg, #003ea0 12%, #0048bb 21%, #6e41d6 91%);
  }
`;

function AskSection() {
  return (
    <SectionBox bgColor={"var(--gray_001)"}>
      <ContainerBox>
        <h2>제안과 의견을 남겨주세요</h2>
        <p>
          간단한 문의는 채팅 상담을 통해 진행하시면
          <br className='mobile' /> 빠르게 답변 받으실 수 있습니다.
        </p>
      </ContainerBox>
      <FormBox>
        <form action='#' method='POST'>
          <InputField>
            <legend className='blind'>문의 폼</legend>
            <label className='blind'>이름</label>
            <input type='text' onChange value='' placeholder='이름' />
            <label className='blind'>전화번호</label>
            <input type='number' onChange value='' placeholder='전화번호' />
            <label className='blind'>이메일</label>
            <input type='mail' onChange value='' placeholder='이메일' />
            <label className='blind'>문의내용</label>
            <textarea placeholder='문의내용을 작성해 주세요' />
          </InputField>
          <LoginField>
            <legend className='blind'>로그인 버튼</legend>
            <input type='submit' onChange value='문의하기' />
          </LoginField>
        </form>
      </FormBox>
    </SectionBox>
  );
}

export default AskSection;
