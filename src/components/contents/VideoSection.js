import styled from "styled-components";
import { SectionBox } from "../common/Common";

const WrapperSection = styled(SectionBox)`
  display: flex;
  height: 236px;
  align-items: center;
  justify-content: center;
  background-image: linear-gradient(116deg, #003ea0 18%, #0048bb 27%, #6e41d6 93%);

  iframe {
    width: 330px;
    height: 186px;
  }

  @media screen and (min-width: 768px) {
    height: 396px;

    iframe {
      width: 592px;
      height: 332px;
    }
  }
`;

function VideoSection() {
  return (
    <WrapperSection bgColor={"transparents"}>
      <h2 className='blind'>장사왕 소개 동영상입니다</h2>
      <iframe
        src='https://www.youtube.com/embed/q01Vomcvzn8'
        title='YouTube video player'
        frameborder='0'
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        allowfullscreen
      ></iframe>
    </WrapperSection>
  );
}

export default VideoSection;
