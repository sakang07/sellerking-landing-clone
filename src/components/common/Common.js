import styled from "styled-components";

export const SectionBox = styled.section`
  width: ${props => props.width || "100%"};
  height: ${props => props.height || "auto"};
  background-color: ${props => props.bgColor || "var(--white)"};

  .pc {
    display: none;
  }

  @media screen and (min-width: 768px) {
    .mobile {
      display: none;
    }
    .pc {
      display: block;
    }
  }
`;

export const BoldSpan = styled.span`
  font-weight: var(--bold);
`;
