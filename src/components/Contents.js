import AskSection from "./contents/AskSection";
import DetailViewSection from "./contents/DetailViewSection";
import ListSection from "./contents/ListSection";
import VideoSection from "./contents/VideoSection";
import ViewSection from "./contents/ViewSection";

function Contents() {
  return (
    <>
      <ViewSection />
      <ListSection />
      <DetailViewSection />
      <VideoSection />
      <AskSection />
    </>
  );
}

export default Contents;
