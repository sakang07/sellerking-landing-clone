import styled from "styled-components";
import { BoldSpan } from "../common/Common";

const FooterBox = styled.footer`
  width: 100%;
  margin-bottom: 56px;
`;

const SecondHeading = styled.h2`
  display: block;
  width: 116px;
  height: 30px;
  margin: 56px auto 0;

  a {
    background-image: url("/assets/logo-gray.svg");
    background-size: contain;
  }

  @media screen and (min-width: 768px) {
    display: none;
  }
`;

const Paragraph = styled.p`
  margin-top: 16px;
  font-size: 18px;
  text-align: center;

  @media screen and (min-width: 768px) {
    margin-top: 42px;
  }
`;

const AddressBox = styled.div`
  max-width: 300px;
  margin: 20px auto 0;
  text-align: center;

  p,
  address {
    display: inline-block;
    margin: 3px auto;
    font-size: 12px;
    color: var(--gray_100);

    &:nth-child(2)::before,
    &:nth-child(5)::before {
      content: "";
      display: inline-block;
      width: 1px;
      height: 10px;
      margin: 0 12px;
      background-color: var(--gray_100);
    }
  }
`;

const CopyrightBox = styled.div`
  margin-top: 16px;

  p {
    font-size: 12px;
    text-align: center;
    color: var(--gray_100);
  }
`;

function Footer() {
  return (
    <FooterBox>
      <SecondHeading className='blind_wrap'>
        <a href='#top'>
          <span>장사왕 로고</span>
        </a>
      </SecondHeading>
      <Paragraph>(주)바닐라브레인</Paragraph>
      <AddressBox>
        <p>대표 : 윤도선</p>
        <p>사업자등록번호 : 579-81-02162</p>
        <address>주소 : 경기도 고양시 일산동구 백마로 195, 9012-4</address>
        <address>(고양경기문화창조허브)</address>
        <p>
          고객문의 : <a href='mailto:help@sellerking.io'>help@sellerking.io</a>
        </p>
      </AddressBox>
      <CopyrightBox>
        <p>
          Copyright &copy; <BoldSpan>VanillaBrain</BoldSpan> Inc. All rights reserved.
        </p>
      </CopyrightBox>
    </FooterBox>
  );
}

export default Footer;
