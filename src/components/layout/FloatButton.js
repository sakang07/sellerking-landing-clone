import styled from "styled-components";

const FloatBox = styled.div`
  position: fixed;
  z-index: 1000;
  left: 0;
  right: 0;
  bottom: 10px;
  z-index: 100;
  height: 66px;
  margin: auto;

  & a {
    display: flex;
    justify-content: center;
    align-items: center;
    width: calc(100% - 20px);
    height: calc(100% - 10px);
    margin: auto;
    background: linear-gradient(90.51deg, #06bef8 0.35%, #601aff 87.69%);
    box-shadow: 4px 4px 15px rgb(15 0 184 / 20%), -4px -5px 18px rgb(0 96 209 / 20%);
    border-radius: 4px;
    font-weight: var(--bold);
    color: var(--white);
  }
`;

function FloatButton() {
  return (
    <FloatBox>
      <a href='#top'>무료 베타 시작하기</a>;
    </FloatBox>
  );
}

export default FloatButton;
