import styled from "styled-components";

const HeaderBox = styled.header`
  position: fixed;
  z-index: 1000;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 60px;
  padding: 0 16px;
  background-color: transparent;
  color: var(--white);
`;

const FirstHeading = styled.h1`
  display: flex;
  align-items: center;
  width: 86px;
  height: 100%;
  color: #000;
  a {
    background-image: url("/assets/logo-white.svg");
    background-size: contain;
  }

  @media screen and (min-width: 768px) {
    width: 120px;
    a {
      background-image: url("/assets/logo-beta-white.svg");
    }
  }
`;

const MarginCalculaterBox = styled.div`
  display: flex;
  gap: 20px;
  font-weight: var(--bold);
  font-size: 14px;
  color: var(--white);

  a {
    display: block;
    align-items: center;
    padding: 12px 0;
    padding-left: 24px;
    background-image: url("/assets/ic-calculation.svg");
    background-repeat: no-repeat;
    background-position: 0 50%;

    &:last-child {
      display: none;
    }
  }

  @media screen and (min-width: 768px) {
    font-size: 12px;

    a {
      &:last-child {
        display: block;
        padding: 12px 14px;
        background-image: none;
        background-color: var(--white);
        border-radius: 18px;
        color: var(--blue_300);
      }
    }
  }
`;

function Navigation() {
  return (
    <HeaderBox>
      <FirstHeading className='blind_wrap'>
        <a href='#top'>
          <span>장사왕 로고</span>
        </a>
      </FirstHeading>
      <MarginCalculaterBox>
        <a href='#top'>마진율 계산기</a>
        <a href='#top'>무료 베타 시작하기</a>
      </MarginCalculaterBox>
    </HeaderBox>
  );
}

export default Navigation;
