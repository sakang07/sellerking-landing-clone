import styled from "styled-components";
import Navigation from "./Navigation";
import FloatButton from "./FloatButton";
import Footer from "./Footer";
import { useMediaQuery } from "usehooks-ts";

const WrapperBox = styled.div`
  width: 100%;
`;

function Layout(props) {
  const matches = useMediaQuery("(min-width: 768px)");

  return (
    <WrapperBox>
      <Navigation />
      {!matches ? <FloatButton /> : null}
      {props.children}
      <Footer />
    </WrapperBox>
  );
}

export default Layout;
